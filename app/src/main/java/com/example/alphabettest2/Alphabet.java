package com.example.alphabettest2;


import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class Alphabet extends AppCompatActivity {


    private ImageView img1, img2, img3, img4, randomimage1, randomimage2, randomimage3, randomimage4, wrongimage1, wrongimage2, wrongimage3, wrongimage4, correct;

    private ImageButton soundbutton;

    private Random random = new Random();

    // ProgressDialog mProgressDialog;

    private Button btn_back;

    Intent intent = new Intent();

    ArrayList<HashMap<String, String>> arraylist;

    private ArrayList<Integer> trainednumber = new ArrayList<Integer>();

    private ArrayList<Integer> usednumber = new ArrayList<Integer>();

    private ArrayList<ImageView> usedimage = new ArrayList<ImageView>();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alphabet);


        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        final Check model = getIntent().getExtras().getParcelable("check");

        img1 = findViewById(R.id.image1);
        img2 = findViewById(R.id.image2);
        img3 = findViewById(R.id.image3);
        img4 = findViewById(R.id.image4);

  /*      wrongimage1 = findViewById(R.id.wrong);
        wrongimage2 = findViewById(R.id.wrong2);
        wrongimage3 = findViewById(R.id.wrong3);
        wrongimage4 = findViewById(R.id.wrong4);
*/
        correct = findViewById(R.id.correct);
        correct.setVisibility(View.INVISIBLE);

        //  btn_back=findViewById(R.id.btn_back);

        final Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);


        final List<ImageView> all = new ArrayList<>();
        all.add(img1);
        all.add(img2);
        all.add(img3);
        all.add(img4);

        soundbutton = findViewById(R.id.soundButtonalphabet);


        int index = random.nextInt(all.size());
        randomimage1 = all.get(index);
        all.remove(index);

        index = random.nextInt(all.size());
        randomimage2 = all.get(index);
        all.remove(index);

        index = random.nextInt(all.size());
        randomimage3 = all.get(index);
        all.remove(index);

        randomimage4 = all.get(0);

        // first num
        int rand1 = generateRandomIntIntRange(0, model.getImages().size() - 1);

        for (int i = 0; i < trainednumber.size(); i++) {
            if (rand1 == trainednumber.get(i)) {

                rand1 = generateRandomIntIntRange(0, model.getImages().size() - 1);
                break;

            }
        }

        trainednumber.add(rand1);
        usednumber.add(rand1);

        String qimage1 = model.getImages().get(rand1); // image url random

        Log.d("Test1", qimage1);

        usedimage.add(randomimage1);


        // 2nd num
        int rand2 = generateRandomIntIntRange(0, model.getImages().size() - 1);
        for (int i = 0; i < 1; i++) {
            if (usednumber.get(i) == rand2) {
                rand2 = generateRandomIntIntRange(0, model.getImages().size() - 1);
                i = 0;
            }
        }

        usednumber.add(rand2);

        String qimage2 = model.getImages().get(rand2); // image url random

        Log.d("Test2", qimage2);
        // 3nd num
        int rand3 = generateRandomIntIntRange(0, model.getImages().size() - 1);

        for (int i = 0; i < 2; i++) {
            if (usednumber.get(i) == rand3) {
                rand3 = generateRandomIntIntRange(0, model.getImages().size() - 1);
                i = 0;
            }
        }
        usednumber.add(rand3);

        usedimage.add(randomimage3);

       /* for(int i=0 ; i<2 ; i++){

            if (randomimage3 == all[i]){

                i=0;
            }
        }*/
        usedimage.add(randomimage3);
        String qimage3 = model.getImages().get(rand3); // image url random

        Log.d("Test3", qimage3);
        // 4nd num
        int rand4 = generateRandomIntIntRange(0, model.getImages().size() - 1);

        for (int i = 0; i < 3; i++) {
            if (usednumber.get(i) == rand4) {
                rand4 = generateRandomIntIntRange(0, model.getImages().size() - 1);
                i = 0;
            }
        }


        String qimage4 = model.getImages().get(rand4); // image url random
        Log.d("Test4", qimage4);


        Glide.with(this).load(qimage1).into(randomimage1);
        Glide.with(this).load(qimage2).into(randomimage2);
        Glide.with(this).load(qimage3).into(randomimage3);
        Glide.with(this).load(qimage4).into(randomimage4);


        //win
        randomimage1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  Intent intent= new Intent( Alphabet.this, Alphabet.class);
                finish();*/
                correct.setVisibility(View.VISIBLE);
                correct.startAnimation(myAnim);
                startActivity(getIntent());
                finish();


            }
        });
        randomimage2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                randomimage2.setVisibility(View.INVISIBLE);

            }
        });
        randomimage3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                randomimage3.setVisibility(View.INVISIBLE);


            }
        });
        randomimage4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                randomimage4.setVisibility(View.INVISIBLE);

            }
        });


        // play sound
        final MediaPlayer mediaplayer;

        mediaplayer = new MediaPlayer();

        mediaplayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        final String qvoice = model.getSounds().get(rand1); // sound url random
        //sound without click
        try {
            mediaplayer.setDataSource(qvoice);
            mediaplayer.prepare();


        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mediaplayer.start();

        soundbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    mediaplayer.setDataSource(qvoice);
                    mediaplayer.prepare();


                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (SecurityException e) {
                    e.printStackTrace();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                mediaplayer.start();

            }
        });


    }


    // round function
    public static int generateRandomIntIntRange(int min, int max) {
        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            intent.setClass(Alphabet.this, MainActivity.class);
            startActivity(intent);
            Alphabet.this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}

