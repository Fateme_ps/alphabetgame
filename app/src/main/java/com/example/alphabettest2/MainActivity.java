package com.example.alphabettest2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements com.example.alphabettest2.MyRecyclerViewAdapter.onItemClickListener {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter; // here
    private RecyclerView.LayoutManager mLayoutManager;
    private static String TAG = "CardViewActivity";
   // private int[] img = {R.drawable.alphabeticon};
  //  private Check checks;



    private ArrayList<Check> checks = null;




    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);




        //image
       // ImageView imageView = findViewById(R.id.imageButtonalphabet);

      //  Glide.with(this).load("/").into(imageView);

        mRecyclerView = findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
/*
        mAdapter = new MyRecyclerViewAdapter(getDataSet(), new MyRecyclerViewAdapter.onItemClickListener() {
            @Override
            public void onItemClickListener(int position) {

                Intent intent = new Intent(MainActivity.this, Alphabet.class);
                startActivity(intent);

            }
        });
*/
        mRecyclerView.setAdapter(mAdapter);
        parseJson();

    }


 /*  private ArrayList<DataObject> getDataSet() {
        ArrayList results = new ArrayList<DataObject>();
        //  for (int index = 0; index < 20; index++) {
        DataObject obj = new DataObject(,"slm");
        results.add(obj);
        //   }
        return results;
    }*/


    @Override
    public void onItemClickListener(int position, Check model) {

        Intent intent = new Intent(MainActivity.this, Alphabet.class);
        intent.putExtra("check", model);
        startActivity(intent);
    }

    private void parseJson() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Api request = retrofit.create(Api.class);
        Call<DataResponse> call1 = request.getJson();
        call1.enqueue(new Callback<DataResponse>() {
            @Override
            public void onResponse(Call<DataResponse> call, Response<DataResponse> response) {
         //       mProgressBar.setVisibility(View.GONE);
                if (response.isSuccessful() && response.body()!=null) {
                    DataResponse body = response.body();
                    if (body.isError()){

                        Toast.makeText(MainActivity.this,"Oops! Something went wrong!",Toast.LENGTH_SHORT).show();

                    }else {

                        checks = body.getData();
                        mAdapter=new MyRecyclerViewAdapter(checks,MainActivity.this, MainActivity.this);
                        mRecyclerView.setAdapter(mAdapter);
                    }


                }


            }

            @Override
            public void onFailure(Call<DataResponse> call, Throwable t) {
                Toast.makeText(MainActivity.this,"Oops! Something went wrong!",Toast.LENGTH_SHORT).show();

            }


        });
    }

// image glide

/*    @Override public View getView(int position, View recycled, ViewGroup container) {
        final ImageView myImageView;
        if (recycled == null) {
            myImageView = inflater.inflate(R.layout., container, false);
        } else {
            myImageView = (ImageView) recycled;
        }

        String url = myUrls.get(position);

        Glide
                .with(myFragment)
                .load(url)
                .centerCrop()
                .placeholder(R.drawable.loading_spinner)
                .into(myImageView);

        return myImageView;
    }*/



}
