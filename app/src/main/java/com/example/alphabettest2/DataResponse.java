package com.example.alphabettest2;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DataResponse {

    @SerializedName("error")
    private boolean error;

    @SerializedName("data")
    private ArrayList<Check> data;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public ArrayList<Check> getData() {
        return data;
    }

    public void setData(ArrayList<Check> data) {
        this.data = data;
    }
}
