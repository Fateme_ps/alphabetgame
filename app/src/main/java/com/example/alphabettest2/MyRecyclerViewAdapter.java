package com.example.alphabettest2;


import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class MyRecyclerViewAdapter extends RecyclerView
        .Adapter<MyRecyclerViewAdapter
        .DataObjectHolder> {

    // private static String LOG_TAG = "MyRecyclerViewAdapter";

  //  private ArrayList<DataObject> mDataset;

    private onItemClickListener mListener;
    private ArrayList<Check> checks = null;
    private Context context;
    private ImageView imagebannner;

    public MyRecyclerViewAdapter(ArrayList<Check> checks, Context context, MyRecyclerViewAdapter.onItemClickListener mListener) {

        this.context=context;
        this.checks = checks;
        this.mListener = mListener;


    }


    public class DataObjectHolder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        TextView title;

        onItemClickListener onItemClickListener;



        public DataObjectHolder(View itemView , onItemClickListener onItemClickListener) { // final onItemClickListener listener
            super(itemView);
            title = itemView.findViewById(R.id.textView);
            imagebannner = itemView.findViewById(R.id.imagebanner);
            //   Log.i(LOG_TAG, "Adding Listener");
            this.onItemClickListener = onItemClickListener;
            itemView.setOnClickListener(this);



        }

        @Override
        public void onClick(View v) {
            if (null != onItemClickListener){

                onItemClickListener.onItemClickListener(getAdapterPosition(), checks.get(getAdapterPosition()));
            }

        }
    }


  /*  public MyRecyclerViewAdapter(ArrayList<DataObject> myDataset, onItemClickListener onItemClickListener) {
        mDataset = myDataset;
        this.mListener = onItemClickListener;
    }*/


    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_row, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view , mListener);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder (DataObjectHolder holder, final int position) {
       /* holder.label.setText(mDataset.get(position).getmText1());
        holder.dateTime.setText(mDataset.get(position).getmText2());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null)
                    mListener.onItemClickListener(position);
                //  int position = getAdapterPosition();

            }
        });*/ // ?


        final String title= checks.get(position).getTitle();
        holder.title.setText(title);
        final String banner=checks.get(position).getBanner();
        Glide.with(context).load(banner).into(imagebannner);


    //    holder.banner.setImageURI(Uri.parse(banner));


     //   holder.banner.setText(banner);
      //  holder.title.setText((mDataset.get(position).getmText1()));


    }

    //useless
/*
    public void addItem(DataObject dataObj, int index) {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }*/

    @Override
    public int getItemCount() {
        return checks.size();
    }

    public interface onItemClickListener{

        void onItemClickListener(int position, Check model);


    }
}
