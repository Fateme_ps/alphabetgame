package com.example.alphabettest2;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class Check implements Parcelable {

    @SerializedName("id")
    private int id;


    @SerializedName("title")
    private String title;

    @SerializedName("banner")
    private String banner;

    @SerializedName("images")
    private ArrayList<String> images;

    @SerializedName("sound")
    private ArrayList<String> sounds;

    protected Check(Parcel in) {
        title = in.readString();
        banner = in.readString();
        images = in.createStringArrayList();
        sounds = in.createStringArrayList();
        id = in.readInt();
    }

    public static final Creator<Check> CREATOR = new Creator<Check>() {
        @Override
        public Check createFromParcel(Parcel in) {
            return new Check(in);
        }

        @Override
        public Check[] newArray(int size) {
            return new Check[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public ArrayList<String> getSounds() {
        return sounds;
    }

    public void setSounds(ArrayList<String> sounds) {
        this.sounds = sounds;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(banner);
        dest.writeStringList(images);
        dest.writeStringList(sounds);
        dest.writeInt(id);
    }
}
