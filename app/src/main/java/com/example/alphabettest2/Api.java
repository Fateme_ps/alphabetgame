package com.example.alphabettest2;

import retrofit2.http.GET;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.Query;

public interface Api {

    String BASE_URL = "http://landing.cloudns.org/data/";

    @GET("data.json")
    Call<DataResponse> getJson();


}
